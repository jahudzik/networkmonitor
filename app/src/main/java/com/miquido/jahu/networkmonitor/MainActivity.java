package com.miquido.jahu.networkmonitor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    private static final String CONNECTIVITY_CHANGE_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";

    private ConnectivityManager connectivityManager;
    private WifiManager wifiManager;
    private BroadcastReceiver broadcastReceiver;
    private IntentFilter networkIntentFilter;

    private List<String> networksList = new ArrayList<>();
    private ArrayAdapter<String> adapter;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, networksList);
        listView = (ListView) findViewById(R.id.list_view);
        listView.setAdapter(adapter);
        findViewById(R.id.button_check).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNetworkEntry();
            }
        });
        broadcastReceiver = new NetworkStateReceiver();
        networkIntentFilter = new IntentFilter(CONNECTIVITY_CHANGE_ACTION);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, networkIntentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    private void addNetworkEntry() {
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        String networkEntry = "no network";
        if (isConnected) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                networkEntry = (wifiInfo != null) ? "wifi (" + wifiInfo.getSSID() + ")" : "wifi (unknown)";
            } else {
                networkEntry = activeNetwork.getTypeName();
            }
        }
        networksList.add(networkEntry);
        adapter.notifyDataSetChanged();
        listView.smoothScrollToPosition(adapter.getCount() - 1);
    }

    public class NetworkStateReceiver extends BroadcastReceiver {

        public NetworkStateReceiver() {}

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(CONNECTIVITY_CHANGE_ACTION)) {
                addNetworkEntry();
            }
        }

    }

}
